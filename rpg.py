# -*- coding: utf-8 -*-

import os

def limpa_tela():
    os.system('cls' if os.name == 'nt' else 'clear')

class Personagem:
    def __init__(self):
        self.nome = ''
        self.historia = ''
        self.raca = 0
        self.profissao = 0
        self.alinhamento = 0
        self.vida = 0
        self.meio = 0
        self.meta = 0
        self.porte = 0
        self.forca = 0
        self.destreza = 0
        self.inteligencia = 0
        self.constituicao = 0
        self.atributo_principal = 0
        self.dificuldade = 0

personagem = Personagem()

def obter_entrada_int(mensagem):
    while True:
        entrada = input(mensagem)
        if entrada.isdigit():
            return int(entrada)
        else:
            print("Por favor, insira um número válido.")

def criacao_de_personagem():
    nome = input("Qual o nome do seu personagem?")
    personagem = Personagem()
    personagem.nome = nome
    limpa_tela()

    #personagem = Personagem()
    while(personagem.raca != 1 and personagem.raca != 2 and personagem.raca != 3):
        print(f"Qual é a sua raça, {nome}: ")
        print("1-Anão")
        print("2-Elfo")
        print("3-Humano")
        personagem.raca = obter_entrada_int(f"Qual é a sua raça, {nome}: ")

        if(personagem.raca != 1 and personagem.raca != 2 and personagem.raca != 3):

            print("raca invalida por favor escolha uma raca valida")


    limpa_tela()

    #personagem = Personagem()
    while(personagem.profissao != 1 and personagem.profissao != 2 and personagem.profissao != 3):
        print (f"Qual sua profissão,{nome}: ")
        print ("1- Guerreiro")
        print ("2- mago")
        print ("3- Ladino")
        personagem.profissao = obter_entrada_int(f"Qual é a sua profissão, {nome}: ")
        
        if(personagem.profissao != 1 and personagem.profissao != 2 and personagem.profissao != 3):

            print("profissao invalida por favor escolha uma profissao")

    limpa_tela()

    #personagem = Personagem()
    while(personagem.alinhamento != 1 and personagem.alinhamento != 2 and personagem.alinhamento != 3):
        print (f"Qual sera o seu alinhamento,{nome}: ")
        print ("1- Bom")
        print ("2- Mau")
        print ("3- Neutro")
        personagem.alinhamento = obter_entrada_int(f"Qual é o seu alinhamento, {nome}: ")
    

        if(personagem.alinhamento != 1 and personagem.alinhamento != 2 and personagem.alinhamento != 3):

            print("alinhamento invalido por favor escolha um valido")

    limpa_tela()

    #personagem = Personagem()
    while((personagem.profissao == 3) and (personagem.alinhamento != 2) and (personagem.alinhamento != 3)):

        limpa_tela()

        print ("Um ladino não pode ter alinhamento bom escolha seu alinhamento novamente: ")
        print ("2- Mau")
        print ("3- Neutro")
        personagem.alinhamento = obter_entrada_int(f"Qual é o seu alinhamento, {nome}: ")

    limpa_tela()

    #personagem = Personagem()
    while(personagem.meta != 1 and personagem.meta != 2 and personagem.meta != 3):

        print (f"Qual sua meta,{nome}: ")
        print ("1- Destruir o mal")
        print ("2- Ficar rico")
        print ("3- Governar o reino")
        personagem.meta = obter_entrada_int(f"Qual é a sua meta, {nome}: ")

        if(personagem.meta != 1 and personagem.meta != 2 and personagem.meta != 3):

            print("meta invalida por favor escolha uma valida")

    limpa_tela()

    #personagem = Personagem()
    while((personagem.alinhamento == 2) and (personagem.meta != 2) and (personagem.meta != 3)):

        limpa_tela()
        print ("Voce não pode querer acabar com o mal se voce é do mal. Escolha novamente sua meta:")
        print ("2- Ficar rico")
        print ("3- Governar o reino")
        personagem.meta = obter_entrada_int(f"Qual é a sua meta, {nome}: ")
        limpa_tela()
    #personagem = Personagem()
    if ( personagem.profissao != 3 ):
        print (f"Aonde voce vive,{nome}:")
        print ("1- Cidades")
        print ("2- Campos")
        print ("3- Tribos")
        personagem.profissao = obter_entrada_int(f"Qual é a sua profissao, {nome}: ")  
    else:
        limpa_tela()
        print (" Se voce é um Ladino, so pode viver no meio urbano.")
        onde_vive = 1
        limpa_tela() 
    #personagem = Personagem()
    while(personagem.porte != 1 and personagem.porte != 2 and personagem.porte != 3):
        print ("Qual o seu porte")
        print ("1- Grande")
        print ("2- Médio")
        print ("3- Pequeno")
        personagem.porte = obter_entrada_int(f"Qual é o seu porte, {nome}: ") 
    limpa_tela()

    if(personagem.porte != 1 and personagem.porte != 2 and personagem.porte != 3):

        print("porte invalido por favor escolha um valido")

    limpa_tela()
    #personagem = Personagem()
    while((personagem.raca == 1) and (personagem.porte != 2) and (personagem.porte != 3)):

        print ("Se voce é um Anão seu porte não pode ser grande. Escolha novamente seu porte:")
        print ("2- Médio")
        print ("3- Pequeno")
        personagem.porte = obter_entrada_int(f"Qual é o seu porte, {nome}: ")
        
        limpa_tela()
    #personagem = Personagem()
    while((personagem.forca + personagem.constituicao + personagem.destreza + personagem.inteligencia) > 10 or (personagem.forca + personagem.constituicao + personagem.destreza + personagem.inteligencia) < 1):
        personagem = Personagem()
        while(personagem.forca < 1 or personagem.forca > 5):
            print(f"Escolha sua força,{nome}(com valores de 1 a 5),{nome}:")
            personagem.forca = obter_entrada_int(f"Qual é a sua força, {nome}: ")
            if(personagem.forca < 1 or personagem.forca > 5):
                print("Valor inválido por favor digite um valor válido")
            
            if(personagem.raca == 1):
                personagem.forca = personagem.forca + 1
            
            if(personagem.profissao == 1):
                personagem.atributo_principal = personagem.forca
            
        limpa_tela()
        #personagem = Personagem()
        while(personagem.destreza < 1 or personagem.destreza > 5):
            print(f"Escolha sua destreza,{nome}(com valores de 1 a 5),{nome}:")
            personagem.destreza = int(input())
            if(personagem.destreza < 1 or personagem.destreza > 5):
                print("Valor inválido por favor digite um valor válido")
            
            if(personagem.raca == 1):
                personagem.destreza = personagem.destreza - 1
            
            if(personagem.raca == 2):
                personagem.destreza = personagem.destreza + 1
            
            if(personagem.profissao == 3):
                personagem.atributo_principal = personagem.destreza
            
        
        limpa_tela()

        #personagem = Personagem()
        while(personagem.inteligencia < 1 or personagem.inteligencia > 5):
            print(f"Escolha sua inteligência,{nome}(com valores de 1 a 5):")
            personagem.inteligencia = obter_entrada_int(f"Qual é a sua inteligência, {nome}: ")
            if(personagem.inteligencia < 1 or personagem.inteligencia > 5):
                print("Valor inválido por favor digite um valor válido")
            
            if(personagem.profissao ==2):
                personagem.atributo_principal = personagem.inteligencia
            
        
        limpa_tela()

        #personagem = Personagem()
        while(personagem.constituicao < 1 or personagem.constituicao > 5):
            print(f"Escolha sua constituição,{nome}(com valores de 1 a 5)")
            personagem.constituicao = obter_entrada_int(f"Qual é a sua constituição, {nome}: ")
            if(personagem.constituicao < 1 or personagem.constituicao > 5):
                print("Valor inválido por favor digite um valor válido")
            
            if(personagem.raca == 2):
                personagem.constituicao = personagem.constituicao - 1
            

        limpa_tela()

        
        if((personagem.forca + personagem.constituicao + personagem.destreza + personagem.inteligencia) > 10 or (personagem.forca + personagem.constituicao + personagem.destreza + personagem.inteligencia) < 1):
            print("A soma de seus pontos de atributo não pode ultrapassar os 10 pontos por favor redistribua seus pontos")

        limpa_tela()

        print (f"Qual sua historia até aqui,{nome}")
        personagem.historia = input()
        limpa_tela()
        #personagem = Personagem()
        while(personagem.dificuldade < 1 or personagem.dificuldade > 3):
            print("Escolha o nível de dificuldade de sua aventura:")
            print("1 - Fácil")
            print("2 - Mediano")
            print("3 - Difícil")
            personagem.dificuldade = int(input())
    return personagem

    limpa_tela()

def historia():
    hp = 100 #passo1, passo2,passo3,passo4
    
    limpa_tela()
    print ("A aventura, a qual os aventureiros vão enfrentar, se resume mas não se limita apenas na investigação de uma lenda que é contada na cidade na qual os aventureiros se encontram. Essa lenda sugere que haveria uma entidade maligna em uma caverna nas proximidades do reino onde os aventureiros se encontram no inicio da historia. Nesta caverna, as historias afirmam que essa entidade maligna matou alguns guardas a serviço do rei. É dito também que aquele que conseguir solucionar os três enigmas impostos por essa criatura e conseguir entrar em seu santuário tera direito a um desejo sem restrições.")

    print ("O inicio de sua jornada se da com uma escolha importante, uma vez que seu objetivo é conseguir alcançar esse desejo sem restrições. qual será o caminho para entrar na caverna: ")

    print ("1- Entrada na encosta da montanha ")
    print ("2- Entrada no esgoto do reino ")
    print ("3- Entrada na caverna de ouro abandonada ")

    passo1 = int(input())
    limpa_tela()

    if (passo1 == 1):

        print ("O caminho pela encosta da montanha foi tranquilo em grande parte do tempo, ate que, próximo a entrada, voce se depara com um pequeno grupo com tres bandidos. Qual sera sua proxima ação:")

        print ("1- Esgueirar-se pelos arbustos")
        print ("2- Ataca-los com o elemento surpresa")
        print ("3- Correr sem olhar para trás")

        passo2 = int(input())
        limpa_tela()
        if (passo2 == 1):
            print("ao se esgueirar você consegue passar por eles sem ser percebido e entra na caverna.")
            print("ao chegar ao final da passagem você se depara com um caixão marrom velho e empoeirado.")
        

        if (passo2 == 2):
            print("ao ataca-los com o elemento surpresa você mata um dos bandidos e os outros dois fogem de medo, correndo para longe da entrada da caverna.")
            print("ao entrar na caverna e chegar ao final da primeira passagem você se depara com um caixão marrom velho e empoeirado.")
        

        if (passo2 == 3):
            print("você corre loucamente balançando os braços e gritando eles lhe notam porem acabam ficando com medo de uma pessoa tão estranha correndo e simplesmente deixam você ir em frente e entrar na caverna")
            print("ao chegar ao final da primeira passagem você se depara com um caixão marrom velho e empoeirado.")
    
    if (passo1 == 2):

        print ("O caminho pelos esgotos é irregular e muito escuro, a única iluminação que voce possui é sua tocha. Depois de caminhar durante uma hora voce se depara com tres caminhos diferentes, cada um com um som diferente. Qual caminho voce quer seguir:")

        print ("1- Rugidos de animais ferozes")
        print ("2- Silencio total")
        print ("3- Cascatas de água")

        passo2 = int(input())
        limpa_tela()
        if (passo2 == 1):
            print("ao entrar neste caminho você percebe que não passava de crianças lhe pregando uma peça.")
            print("ao chegar ao final da passagem você se depara com um caixão marrom velho e empoeirado.")
        

        if (passo2 == 2):
            print("ao seguir por este caminho nada acontece, você apenas passa por um corredor muito tenebroso porem que não representa risco a sua pessoa.")
            print("ao chegar ao final da passagem você se depara com um caixão marrom velho e empoeirado.")
        

        if (passo2 == 3):
            print("ao seguir por onde se ouvia água você se depara com o corredor mais escuro que você ja viu na sua vida e percebe que está andando com água corrente a passar pelos seus pés. mais a frente você quase cai de um penhasco que com certeza o teria matado e percebe que não ha como continuar por ali, porem ao analisar um pouco a região você percebe que ha outra rota mais ao lado no mesmo corredor.")
            print("ao chegar ao final da passagem você se depara com um caixão marrom velho e empoeirado.")
        
    if (passo1 == 3):

        print("A mina abandonada ainda esta em boas condições no inicio da exploração, mas com o decorrer do tempo as passagens vao se tornando escuras e estreitas, ate que voce entra em um antigo cemitério dos nórdicos e percebe que alguns mortos não estão totalmente mortos... Qual sua decisão: ")

        print ("1- Esgueirar-se pelas sombras")
        print ("2- Enfrentar os Zumbis")
        print ("3- Distraí-los com pedras e correr")

        passo2 = int(input())
        limpa_tela()
        if (passo2 == 1):
            print("ao se esgueirar você passa sem dificuldades pelos zumbis e segue seu caminho.")
            print("ao chegar ao final da passagem você se depara com um caixão marrom velho e empoeirado.")
        

        if (passo2 == 2):
            print("ao decidir enfrentá-los você nota que eles estão em muitíssimo maior número do que você poderia enfrentar. Eles te cercam completamente e por mais que você lute seu destino à morte.")
            return print("GAME OVER.")
        

        if (passo2 == 3):
            print("ao olhar ao redor você percebe que ha apenas uma pedra por perto para distrai-los. passa muito perto mas você consegue se livrar dos zumbis.")
            print("ao chegar ao final da passagem você se depara com um caixão marrom velho e empoeirado.")
        
    print("diante deste caixão você o analisa rapidamente e percebe que ele esta entreaberto e que tem um feixe de luz fraca saindo de dentro dele, sendo assim você tem 3 alternativas: ")

    print(" 1 - abrir o caixão e entrar nele ")
    print(" 2 - examinar os arredores da câmara onde você se encontra ")
    print(" 3 - dar meia volta, sair da caverna e desistir da missão e do seu desejo sem restrições ")

    passo3 = int(input())
    limpa_tela()

    if(passo3 == 1):
        print("ao abrir e entrar no caixão você se depara com um vulto ao redor do qual vários morcegos estão voando")

    
    if(passo3 == 2):
        print("ao examinar os arredores você percebe um cadáver ao lado do caixão com uma carta sobre ele. ao pegar a carta você lá a seguinte mensagem: ")
        print("Vossa majestade, não sei se um dia você irá ler esta carta. Porem imploro à deusa Kalamista que ela chegue de alguma maneira a você para que todos saibam o que aconteceu e o que tem assombrado nosso reino. Após ser contratado para investigar esta caverna antiga, algo de muito estranho começou a acontecer, algo que eu mesmo ignorei, e que infelizmente só percebi mais tarde o quão ingênuo eu fui. Chegamos a caverna mas isso nos  trouxe apenas	 decepção, traição e morte. Um de meus comandantes matou muitos de meu esquadrão, não sei o que aconteceu, porem seus olhos estavam vermelhos como rubi. Ele dizia estar a espera do despertar do senhor do sangue, aquele maldito!. Eu	o feri de leve enquanto tentava fugir de suas lanças e sua força descomunal, mas não sem ser ferido por ele antes, nesse momento, ouço o grito de meus companheiros e deixo este relato para posterioridade.O maldito usa venenos. Não	vou	conseguir sair	daqui antes que o veneno faça o	seu	trabalho. Meu esquadrão está morto e logo estarei junto deles. Por favor meu senhor me fa....o final da carta se encontra rasgado, e não é possível ler o resto dela")
        print("ao ler essas palavras você percebe que a câmara onde você se encontra esta a desmoronar é sua única saída é entrar no caixão e descer para a câmara secreta que la se encontra. ao descer você se depara com um vulto que esta cercado de morcegos voando ao seu redor.")
    
    if(passo3 == 3):
        print("você se acovarda e corre o mais rápido possível para a saída da caverna,no caminho um pedregulho imenso com o formato esférico começa a rolar na sua direção, você acelera o passo mas no fim chega a um beco sem saída e acaba sendo terrivelmente esmagado pela rocha.")
        return print("GAME OVER")
    
    print("o vulto avança na sua direção e anuncia: você entrou em meu santuário secreto e agora eu o senhor do sangue vou te destruir.")

    print("ele prepara um ataque e avança ainda mais na sua direção o que você faz? :")
    print(" 1 - Desvia ")
    print(" 2 - bloqueia ")

    passo4 = int(input())
    limpa_tela()

    if(passo4 == 1 and personagem.profissao == 3):
        print("você desvia sem dificuldade utilizando suas habilidades de ladino")
        print("o senhor do sangue, após se recompor do ataque fala: você fez uma sábia escolha, isso era apenas um teste para saber quem seria digno de obter o desejo sem restrições e você ate agora foi o único digno a isso.Sendo assim realizarei seu desejo baseado em sua meta de vida.")
        print("você acabou de realizar a sua meta de vida e de terminar o jogo parabéns")
        return True
    if(passo4 == 1 and personagem.profissao == 2):
        print("você não consegue desviar porem você evita um dano crítico usando a esperteza de um mago")
        personagem.vida = 50
        print("você agora está com 50 pontos de vida.")
        print("após o senhor do sangue se recuperar de seu ataque ele se prepara para lhe desferir o golpe final e antes de lhe matar ele diz: você não é digno do desejo sem restrições agora morra.")
        print(" GAME OVER ")
        return False
    if(passo4 == 1 and personagem.profissao == 1):
        print("você não consegue desviar e recebe muito dano ")
        personagem.vida = 40
        print("você agora está com 40 pontos de vida")
        print("após o senhor do sangue se recuperar de seu ataque ele se prepara para lhe desferir o golpe final e antes de lhe matar ele diz: você não é digno do desejo sem restrições agora morra.")
        print(" GAME OVER ")
        return False
    if(passo4 == 2 and personagem.profissao == 3):
        print("você não consegue bloquear e recebe um dano crítico")
        personagem.vida = 20
        print("voce está agora com 20 pontos de vida")
        print("após o senhor do sangue se recuperar de seu ataque ele se prepara para lhe desferir o golpe final e antes de lhe matar ele diz: você não é digno do desejo sem restrições agora morra.")
        print(" GAME OVER ")
        return False
    if(passo4 == 2 and personagem.profissao == 2):
        print("você não consegue bloquear e recebe um dano crítico")
        personagem.vida = 30
        print("voce está agora com 30 pontos de vida")
        print("após o senhor do sangue se recuperar de seu ataque ele se prepara para lhe desferir o golpe final e antes de lhe matar ele diz: você não é digno do desejo sem restrições agora morra.")
        print(" GAME OVER ")
        return False
    if(passo4 == 2 and personagem.profissao == 1):
        print("você consegue bloquear facilmente o ataque do senhor do sangue")
        personagem.vida = 20
        print("o senhor do sangue, após se recompor do ataque fala: você fez uma sábia escolha, isso era apenas um teste para saber quem seria digno de obter o desejo sem restrições e você ate agora foi o único digno a isso.Sendo assim realizarei seu desejo baseado em sua meta de vida.")
        print("você acabou de realizar a sua meta de vida e de terminar o jogo parabéns")
        return True
def main():
    personagem = None  # Inicializando personagem
    while True:
        print("Escolha uma das opções abaixo: ")
        print("1. Novo Jogo. ")
        print("2. Criar um personagem. ")
        print("3. Sair. ")

        opcao = obter_entrada_int("Qual é a sua opção: ")

        if opcao == 1:
            if personagem is None:
                print("Por favor, crie um personagem antes de iniciar o jogo.")
            else:
                resultado = historia()  # Captura o resultado do jogo
                if resultado:  # Se o resultado for True (jogo concluído)
                    print("o senhor do sangue, após se recompor do ataque fala: você fez uma sábia escolha, isso era apenas um teste para saber quem seria digno de obter o desejo sem restrições e você ate agora foi o único digno a isso.Sendo assim realizarei seu desejo baseado em sua meta de vida.")
                    print("você acabou de realizar a sua meta de vida e de terminar o jogo parabéns")
                else:  # Caso contrário (jogo terminou com GAME OVER)
                    print("após o senhor do sangue se recuperar de seu ataque ele se prepara para lhe desferir o golpe final e antes de lhe matar ele diz: você não é digno do desejo sem restrições agora morra.")
                    print(" GAME OVER ")
        elif opcao == 2:
            personagem = criacao_de_personagem()  # Atualizar o objeto personagem com o novo personagem criado

        elif opcao == 3:
            break  # Encerrar o programa

        else:
            print("Opção inválida. Escolha uma opção válida.")
    
main()